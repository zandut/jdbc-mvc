/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Application;
import Models.Mahasiswa;
import Models.Mata_Kuliah;
import Views.MainFrame;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Zandut <mfauzan613110035@gmail.com>
 */
public class ControllerMainFrame implements ActionListener, KeyListener
{

    private Application apps;
    private MainFrame frame;
    private String[] columnMatkul =
    {
        "KODE MATKUL", "NAMA MATKUL"
    };
    private String[] columnMahasiswa =
    {
        "NIM", "NAMA"
    };

    public void initTheme()
    {
        try
        {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels())
            {
                if ("Nimbus".equals(info.getName()))
                {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex)
        {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex)
        {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex)
        {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex)
        {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }

    public ControllerMainFrame()
    {

        apps = new Application();

        initTheme();
        frame = new MainFrame();

        frame.getBtnSimpanMahasiswa().addActionListener(this);
        frame.getBtnSimpanMatkul().addActionListener(this);

        frame.getBtnMenu1().addActionListener(this);
        frame.getBtnMenu4().addActionListener(this);
        frame.getBtnMenu3().addActionListener(this);

        frame.getTableMatkul().addKeyListener(this);
        frame.getTableMatkulDiambil().addKeyListener(this);

        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }

    public void initTable(JTable table, String[] column)
    {
        DefaultTableModel tb = new DefaultTableModel(column, 0)
        {

            @Override
            public boolean isCellEditable(int row, int column)
            {
                return false;
            }

        };

        table.setModel(tb);
    }

    public void addMataKuliahToTable(JTable table, List<Mata_Kuliah> array)
    {
        DefaultTableModel tb = (DefaultTableModel) table.getModel();
        for (Mata_Kuliah mat : array)
        {
            String[] data =
            {
                mat.getKode_matkul(), mat.getNama_matkul()
            };
            tb.addRow(data);
        }
    }

    public void addMahasiswaToTable(JTable table, List<Mahasiswa> array)
    {
        DefaultTableModel tb = (DefaultTableModel) table.getModel();
        for (Mahasiswa mat : array)
        {
            String[] data =
            {
                mat.getNim(), mat.getNama()
            };
            tb.addRow(data);
        }
    }

    public void show(String cardName)
    {
        CardLayout card = (CardLayout) frame.getPanelView().getLayout();
        card.show(frame.getPanelView(), cardName);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        Object button = e.getSource();
        if (button.equals(frame.getBtnMenu1()))
        {
            initTable(frame.getTableMatkul(), columnMatkul);
            initTable(frame.getTableMatkulDiambil(), columnMatkul);

            addMataKuliahToTable(frame.getTableMatkul(), apps.getMataKuliah());
            show("inputMahasiswa");
        }

        if (button.equals(frame.getBtnMenu4()))
        {
            show("inputMatkul");
        }

        if (button.equals(frame.getBtnMenu3()))
        {
            show("lihatMahasiswa");
        }

        if (button.equals(frame.getBtnSimpanMahasiswa()))
        {
            Mahasiswa mhs = apps.getMahasiswa(frame.getTxtNim().getText());
            if (mhs == null)
            {
                try
                {
                    if (apps.insertMahasiswa(new Mahasiswa(frame.getTxtNim().getText(), frame.getTxtNama().getText(), null)))
                    {
                        String nim = frame.getTxtNim().getText();

                        for (int i = 0; i < frame.getTableMatkulDiambil().getRowCount(); i++)
                        {
                            Mahasiswa mhs1 = new Mahasiswa();
                            mhs1.setNim(nim);

                            Mata_Kuliah matkul = new Mata_Kuliah(frame.getTableMatkulDiambil().getValueAt(i, 0).toString(), frame.getTableMatkulDiambil().getValueAt(i, 1).toString());

                            apps.insertMahasiswaMatkul(mhs1, matkul);
                        }

                        frame.showMessage("Insert Berhasil !!");
                    }
                } catch (Exception ex)
                {
                    ex.printStackTrace();
                    frame.showMessage("Insert Gagal !!\n" + ex.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
                }
            } else
            {
                
                try
                {
                    for (int i = 0; i < frame.getTableMatkulDiambil().getRowCount(); i++)
                    {


                        Mata_Kuliah matkul = new Mata_Kuliah(frame.getTableMatkulDiambil().getValueAt(i, 0).toString(), frame.getTableMatkulDiambil().getValueAt(i, 1).toString());

                        apps.insertMahasiswaMatkul(mhs, matkul);
                    }

                    frame.showMessage("Insert Berhasil !!");
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                    frame.showMessage("Insert Gagal !!\n" + ex.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
                }
            }
        }

        if (button.equals(frame.getBtnSimpanMatkul()))
        {
            try
            {
                if (apps.insertMataKuliah(new Mata_Kuliah(frame.getTxtKodeMatkul().getText(), frame.getTxtNamaMatkul().getText())))
                {
                    frame.showMessage("Insert Berhasil");
                }
            } catch (Exception ex)
            {
                ex.printStackTrace();
                frame.showMessage("Insert Gagal !! \n" + ex.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
            }

        }

    }

    @Override
    public void keyTyped(KeyEvent e)
    {

    }

    @Override
    public void keyPressed(KeyEvent e)
    {
        Object component = e.getSource();
        if (component.equals(frame.getTableMatkul()))
        {
            if (e.getKeyCode() == e.VK_ENTER)
            {
                String kode = frame.getTableMatkul().getValueAt(frame.getTableMatkul().getSelectedRow(), 0).toString();
                String nama = frame.getTableMatkul().getValueAt(frame.getTableMatkul().getSelectedRow(), 1).toString();

                Mata_Kuliah m = new Mata_Kuliah(kode, nama);
                List<Mata_Kuliah> array = new ArrayList<>();
                array.add(m);

                addMataKuliahToTable(frame.getTableMatkulDiambil(), array);

            }
        }

        if (component.equals(frame.getTableMatkulDiambil()))
        {
            if (e.getKeyCode() == e.VK_DELETE)
            {

                deleteRow(frame.getTableMatkulDiambil(), frame.getTableMatkulDiambil().getSelectedRow());

            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e)
    {

    }

    public void deleteRow(JTable table, int row)
    {
        DefaultTableModel tb = (DefaultTableModel) table.getModel();
        tb.removeRow(row);
    }

}
