/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import lombok.Data;

/**
 *
 * @author Zandut <mfauzan613110035@gmail.com>
 */
@Data
public class Mata_Kuliah
{
    private String kode_matkul;
    private String nama_matkul;

    public Mata_Kuliah(String kode_matkul, String nama_matkul)
    {
        this.kode_matkul = kode_matkul;
        this.nama_matkul = nama_matkul;
    }

    public Mata_Kuliah()
    {
        
    }
    
    
    
    
}
