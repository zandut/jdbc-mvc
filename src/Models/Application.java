/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import Config.Database;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Zandut <mfauzan613110035@gmail.com>
 */
public class Application
{

    private Database db = new Database();
    private List<Mahasiswa> arrayMahasiswa = new ArrayList<>();
    private List<Mata_Kuliah> arrayMatkul = new ArrayList<>();

    public boolean insertMahasiswa(Mahasiswa m)
    {

        boolean masuk = false;
        db.connect();
        if (db.manipulate("insert into mahasiswa (nim, nama) values ('" + m.getNim() + "',"
                + "'" + m.getNama() + "');") >= 1)
        {
            masuk = true;
        } else
        {
            masuk = false;
        }

        db.disconnect();
        return masuk;

    }

    public boolean insertMataKuliah(Mata_Kuliah matkul)
    {
        boolean masuk = false;
        db.connect();
        if (db.manipulate("insert into mata_kuliah (kode_matkul, nama_matkul) values ('" + matkul.getKode_matkul() + "',"
                + "'" + matkul.getNama_matkul() + "');") >= 1)
        {
            masuk = true;
        } else
        {
            masuk = false;
            
        }

        db.disconnect();
        return masuk;
    }
    
    public boolean insertMahasiswaMatkul(Mahasiswa mhs, Mata_Kuliah matkul)
    {
        boolean masuk = false;
        db.connect();
        if (db.manipulate("insert into relasi (kode_matkul, nim) values ('" + matkul.getKode_matkul() + "',"
                + "'" + mhs.getNim() + "');") >= 1)
        {
            masuk = true;
        } else
        {
            masuk = false;
        }

        db.disconnect();
        return masuk;
    }

    public List<Mahasiswa> getMahasiswa()
    {
        arrayMahasiswa = new ArrayList<>();
        db.connect();
        ResultSet rs = db.get("select * from mahasiswa");
        try
        {
            while (rs.next())
            {
                arrayMahasiswa.add(new Mahasiswa(rs.getString("nim"), rs.getString("nama"), getMataKuliah(rs.getString("nim"))));
            }
            rs.close();
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }

        db.disconnect();

        return arrayMahasiswa;
    }

    public Mahasiswa getMahasiswa(String nim)
    {
        db.connect();
        Mahasiswa mhs = null;
        ResultSet rs = db.get("select * from mahasiswa where nim='" + nim + "'");
        try
        {
            if (rs.first())
            {
                mhs = new Mahasiswa(rs.getString("nim"), rs.getString("nama"), getMataKuliah(nim));
            }
            rs.close();
        } catch (Exception ex)
        {
            ex.printStackTrace();
            
            
        }
        
        db.disconnect();
        
        return mhs;

    }

    public List<Mata_Kuliah> getMataKuliah(String nim)
    {
        db.connect();
        arrayMatkul = new ArrayList<>();
        ResultSet rs = db.get("select * from relasi join mata_kuliah using (kode_matkul) where nim='" + nim + "'");
        try
        {
            while (rs.next())
            {
                arrayMatkul.add(new Mata_Kuliah(rs.getString("kode_matkul"), rs.getString("nama_matkul")));
            }
            rs.close();
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        db.disconnect();
        return arrayMatkul;
    }
    
    public List<Mata_Kuliah> getMataKuliah()
    {
        db.connect();
        arrayMatkul = new ArrayList<>();
        ResultSet rs = db.get("select * from mata_kuliah");
        try
        {
            while (rs.next())
            {
                arrayMatkul.add(new Mata_Kuliah(rs.getString("kode_matkul"), rs.getString("nama_matkul")));
            }
            rs.close();
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        db.disconnect();
        return arrayMatkul;
    }

}
