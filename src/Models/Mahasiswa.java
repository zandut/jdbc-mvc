/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;

/**
 *
 * @author Zandut <mfauzan613110035@gmail.com>
 */
@Data
public class Mahasiswa
{
    private String nim;
    private String nama;
    private List<Mata_Kuliah> daftarMatkul;

    public Mahasiswa(String nim, String nama, List<Mata_Kuliah> array)
    {
        this.nim = nim;
        this.nama = nama;
        this.daftarMatkul = array;
    }

    

    public Mahasiswa()
    {
        
    }
    
    
    
}
